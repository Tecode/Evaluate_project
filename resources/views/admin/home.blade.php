
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>管理员中心</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}"/>
    
</head>
<body>
<div class="topbar-wrap white">
        <div class="topbar-logo-wrap clearfix">
            <h1 class="topbar-logo none"><a href="#" class="navbar-brand">管理员中心</a></h1>
        </div>
    </div>
</div>
<div class="container clearfix">
    <div class="sidebar-wrap">
        <div class="sidebar-title">
            <h1>菜单</h1>
        </div>
        <div class="sidebar-content">
            <ul class="sidebar-list">
                <li>
                    <a href="#"><i class="icon-font">&#xe003;</i>常用操作</a>
                    <ul class="sub-menu">
                        <li><a href="administer-分类管理（标签）.html"><i class="icon-font">&#xe008;</i>评论管理</a></li>
                        <li><a href="administer-公告发布.html"><i class="icon-font">&#xe005;</i>公告发布</a></li>
                        <li><a href="administer-订单查询.html"><i class="icon-font">&#xe006;</i>订单查询</a></li>
                    </ul>
                </li>
                {{--<li>--}}
                    {{--<a href="#"><i class="icon-font">&#xe018;</i>系统管理</a>--}}
                    {{--<ul class="sub-menu">--}}
                        {{--<li><a href="system.html"><i class="icon-font">&#xe017;</i>系统设置</a></li>--}}
                        {{--<li><a href="system.html"><i class="icon-font">&#xe037;</i>清理缓存</a></li>--}}
                        {{--<li><a href="system.html"><i class="icon-font">&#xe046;</i>数据备份</a></li>--}}
                        {{--<li><a href="system.html"><i class="icon-font">&#xe045;</i>数据还原</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>
    <!--/sidebar-->
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="index.html" color="#white">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">评论管理</span></div>
        </div>
        <div class="search-wrap">
            {{--<div class="search-content">--}}
                {{--<form action="#" method="post">--}}
                    {{--<table class="search-tab">--}}
                        {{--<tr>--}}
                            {{--<th width="120">选择分类:</th>--}}
                            {{--<td>--}}
                                {{--<select name="search-sort" id="">--}}
                                    {{--<option value="">全部</option>--}}
                                    {{--<option value="19">国内游</option>--}}
                                     {{--<option value="20">出境游</option>--}}
                                    {{--<option value="21">跟团游</option>--}}
                                    {{--<option value="22">自驾游</option>--}}
                                    {{--<option value="23">景点门票</option>--}}
                                {{--</select>--}}
                            {{--</td>--}}
                            {{--<th width="70">关键字:</th>--}}
                            {{--<td><input class="common-text" placeholder="关键字" name="keywords" value="" id="" type="text"></td>--}}
                            {{--<td><input class="btn btn-primary btn2" name="sub" value="查询" type="submit"></td>--}}
                        {{--</tr>--}}
                    {{--</table>--}}
                {{--</form>--}}
            {{--</div>--}}
        </div>
        <div class="result-wrap">
            <form name="myform" id="myform" method="post">
                <div class="result-title">
                    <div class="result-list">
                        <a id="batchDel" href="javascript:void(0)"><i class="icon-font"></i>批量删除</a>
                        <a id="updateOrd" href="javascript:void(0)"><i class="icon-font"></i>更新排序</a>
                    </div>
                </div>
                <div class="result-content">
                    <table class="result-tab" width="100%">
                        <tr>
                            <th class="tc" width="5%"></th>
                            <th>ID</th>
                            <th>用户</th>
                            <th>评论时间</th>
                            <th>售后评价</th>
                            <th>订单时间</th>
                            <th>操作</th>
                        </tr>
                        @foreach($commentData as $data)
                            <tr>
                                <td class="tc"><input name="id[]" value="" type="checkbox"></td>
                                <td>{{'0'.$data->id}}</td> <!--标签ID-->
                                <td><a target="_blank" href="#">{{$data->name}}</a> <!--课程名称-->
                                </td>
                                <td>{{date('Y-m-s h:i:s',$data->time)}}</td> <!--购买者用户名-->
                                <td>{{$data->content}}</td> <!--销售者用户名-->
                                <td>2016-06-25</td> <!--订单时间-->
                                <td><a style="cursor: pointer" onclick="deleted({{$data->id}})">删除</a></td> <!--课程价格-->
                            </tr>
                        @endforeach
                    </table>
                </div>
            </form>
        </div>
    </div>
    <!--/main-->
</div>
</body>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
<script>
    function deleted(id) {
        $.ajax({
            url: '/admin/home/'+id,
            type: 'POST',
            success: function () {
                alert('删除成功');
                window.location.href = '/admin/home'
            }
        })
    }
</script>
</html>