<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet"  href="http://apps.bdimg.com/libs/bootstrap/3.3.4/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="css/write-style.css" />
		<meta http-equiv="content-type"content="text/html; charset=utf-8" />
		<meta charset="utf-8" />
		<title>旅游评价</title>
		<style type="text/css">
			#header{
           
            height:50px;
            margin:0px;
            position:fixed;
				/*background: deepskyblue;*/
			z-index:30;
			
			
			color:#b0b0b0;
			/*background: steelblue;*/
			top: 0px;
   			 left: 0px;
   			 width: 100%;
            }
			 .add{
            /*padding-top:25px;*/
            margin-top:0px;
            margin-bottom: 50px;
            font-size: x-large;
            color:ghostwhite;
            font:bolder 10px;
            padding-bottom:15px;
            }
		#content-main{
			height: 500px;
		}
        .content-write{
        	margin-left:300px;
        	height: 100px;
        	}
		.content-zhengwen{
			text-align:left;
			border: 1px solid ;
			border-radius: 5px;
			height: 300px;
			width: 500px;
		}
		</style>
	</head>
	<body background="{{asset('imgs/load.jpg')}}">
		<div id="header"><div class="title">
			<a href="/">
				<span class="add" style="width:100%;text-align:center;display:block;">写评价</span>
				</a>
		</div>
				<div class="user">
				<img src="{{asset('imgs/head_portrait.jpeg')}}"  /><span ><a href="{{url('/write')}}><b style="color: slategray;">发布</b></a>
				</span>&nbsp;</div>
		</div>
		
		
		<div id="content-main">
  <div class="content-write">
  		<form action="{{url('/comment')}}" method="post">
    <textarea id="textarea"    name="content" class="content-title"    word-min="2" word-max="50" placeholder="请输入标题" style="height: 50px;width: 500px;">
    
    </textarea>
    <button type="submit">提交</button>
    </form>
    <div  class="content-zhengwen" contenteditable="true" placeholder="请输入标题"   word-min="2">
    	<span class="holdertext"  contenteditable="false">
    		普吉岛是个很美丽的地方，蓝天碧海白沙滩，民风淳朴，处处都很和谐非常适合度假放松心情，那里的水果又便宜又好吃，海鲜也是，不过记得不要赶上斋月去哦……也不要再雨季去，下雨基本不预告的，不过来得快去的也快～帝王岛不太好了，海水已经混了，珊瑚岛什么的其他PP岛还是很清澈的，可以浮浅拿面包喂鱼，很漂亮很可爱，都不认生，对了，记得在珊瑚岛周围浮浅的时候，水无论多浅都不要用脚踩水底，非常容易受伤，无论多严重你当时都感觉不到，哪里水底都是很锋利的，另外去PP岛在外海，尽量别做游艇最前边，一旦起风，前端颠簸太大不是你抓紧就没事的，如果可以要去潜水哦～比水下漫步好很多呢！注意安全，其他的不会让你失望的～ </span>
    </div>

  </div>




	
	
		</div>
		<div id="footer"></div>
	</body>
</html>
