<!DOCTYPE html>

               
<html>
	<head>
		<link rel="stylesheet"  href="http://apps.bdimg.com/libs/bootstrap/3.3.4/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/write-style.css')}}" />
		<meta http-equiv="content-type"content="text/html; charset=utf-8" />
		<title></title>
		<style type="text/css">
			#header{
            background-color:#8B7D6B;
            height:50px;
            margin:0px;
            position:fixed;
				/*background: deepskyblue;*/
			z-index:30;
			
			background:#DCDCDC;
			color:#b0b0b0;
			/*background: steelblue;*/
			top: 0px;
   			 left: 0px;
   			 width: 100%;
            }
			 .add{
            /*padding-top:25px;*/
            margin-top:0px;
            margin-bottom: 50px;
            font-size: x-large;
            color:ghostwhite;
            font:bolder 10px;
            padding-bottom:15px;
            }
            .write_content{
            	
            	overflow:hidden; 
				text-overflow:ellipsis; 
				
				word-break: break-all
				text-align:left;
				height: 20px;
				
				
        }
		</style>
	</head>
	<body>
      <form action="{{url('/comment')}}" method="post" id="form1">
		<div id="header"><div class="title">
			<a href="/">
				<span class="add" style="width:100%;text-align:center;display:block;">评价</span>
				</a>
		</div>
				<div class="user">
				<img src="{{asset('imgs/head_portrait.jpeg')}}"  /><span ><a href="{{url('/write')}}"><b style="color: slategray;">写评论</b></a>
				</span>&nbsp;</div>
		</div>
		
		
		<div id="content-main">
			<div id="content-writed">
				<div id="content-writed-list">
					<div id="content-writed-list-title">
						<h4 class="content-writed-list" ><a class="write_link">
						
				</a></h4><span id="username"><a href="/"><img class="img-logo" src="{{asset('imgs/touxiang_logo.jpg')}}">jayce575
				</a></span>
					</div>
					<img src="{{asset('imgs/nature.jpg')}}" />
					<span class="comment-line"><b>全部评论</b></span><img class="comment-img-line" src="{{asset('imgs/comment-line.png')}}"></img>
				</div>
				
				<comment>
					<div class="comment-img">
                            <img class="comment-img-logo" src="{{asset('imgs/touxiang_logo.jpg')}}" />
                            <span >
                                <input type="text" name="content" class="comment-text"  />
                            </span>
                            <span class="comment-sp-btn">
                                <input style="color: #4c4c4c" type="submit" class="comment-btn" value="评论" />
                            </span>
					</div>
					<!--评论列表-->
					<div class="comment-list">
                        @foreach ($data as $item)
						<div class="comment-list-item">
							<span>
								<img src="{{asset('imgs/head_portrait/girl.jpg')}}" align="left" />
							</span>
								<div class="comment-list-user">
									<span class="comment-content-user">
										{{$item->name}}
									</span>
									<span style="color: #4c4c4c" class="comment-content">
										<p >{{$item->content}}</p>
									</span>
									<div class="comment-time-zan">
										<span style="color: #000" class="comment-time">
										{{date('Y-m-d H:i:s', $item->time)}} 
										</span>
										<span class="comment-zan">
										{{$item->like}} 赞
										</span>
									</div>
								</div>
						</div>
                        @endforeach		
						
					</div>
					
				</comment>
					
			</div>
	
		</div>
		<div id="footer"></div>
		</from>
	</body>
</html>
