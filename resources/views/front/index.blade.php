<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>旅游评价系统</title>
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">                <!-- Font Awesome -->
    <link rel="stylesheet" href="css/bootstrap.min.css">                                      <!-- Bootstrap style -->
    <link rel="stylesheet" type="text/css" href="css/datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
    <link rel="stylesheet" href="css/templatemo-style.css">                                   <!-- Templatemo style -->
</head>

<body>
<div class="tm-main-content" id="top">
    <div class="tm-top-bar-bg"></div>
    <div class="tm-top-bar" id="tm-top-bar">
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-expand-lg narbar-light">
                    <button type="button" id="nav-toggle" class="navbar-toggler collapsed" data-toggle="collapse" data-target="#mainNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div id="mainNav" class="collapse navbar-collapse tm-bg-white">
                        {{ Auth::user() ? '欢迎回来'.Auth::user()->name : '' }}
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link active" href="/">首页 <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/write')}}">评价</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/comment')}}">评价列表</a>
                            </li>
                            @if (Auth::guest())
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/login')}}">登录</a>
                            </li>
                            @else
                                <li class="nav-item">
                                    <a class="nav-link" href="" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">退出登录</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            @endif
                        </ul>
                    </div>
                </nav>
            </div> <!-- row -->
        </div> <!-- container -->
    </div> <!-- .tm-top-bar -->

    <div class="tm-page-wrap mx-auto">

        <section class="p-5 tm-container-outer tm-bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 mx-auto tm-about-text-wrap text-center">
                        <h2 class="text-uppercase mb-4">旅游评价项目</h2>
                        <p class="mb-4">南非在非洲大陆的最南端，西临太平洋，东临印度洋，北部与纳米比亚、博茨瓦纳、津巴布韦接壤，东北部与莫桑比克、斯威士兰为邻。面积1221038平方公里，人口4310万（1999年南非统计局统计）。分黑人、白人、有色人和亚洲人四大种族，分别占总人口的76.7%、10.9%、8.9%和2.6%。黑人主要有祖鲁、科萨、斯威士、茨瓦那等9个部族，主要使用班图语。白人主要是荷兰血统的阿非利卡人和英国血统的白人，语言为阿非利卡语和英语。南非官方语言有11种，英语和阿非利卡语（南非荷兰语）为通用语言。</p>
                        {{--<a href="{{url('/comment')}}" class="text-uppercase btn-primary tm-btn">去评价</a>--}}
                    </div>
                </div>
            </div>
        </section>
        @foreach($indexData as $data)
            @if($loop -> index % 2 == 1)
                <div class="tm-container-outer" id="tm-section-2">
                    <section class="tm-slideshow-section">
                        <div style="width: 700px" class="tm-slideshow">
                            <img style="width: 100%" src="{{'http://47.98.212.129:1009/'.$data->imgData->img_url}}" alt="Image">
                        </div>
                        <div class="tm-slideshow-description tm-bg-primary">
                            <h2 class="">{{$data->title}}</h2>
                            <p>{{$data->describe}}</p>
                            <a href="#" class="text-uppercase tm-btn tm-btn-white tm-btn-white-primary">{{date('Y年m月s日',$data->timestamp)}}</a>
                        </div>
                    </section>
                @else
                        <section class="clearfix tm-slideshow-section tm-slideshow-section-reverse">

                            <div style="width: 700px" class="tm-right tm-slideshow tm-slideshow-highlight">
                                <img style="width: 100%" src="{{'http://47.98.212.129:1009/'.$data->imgData->img_url}}" alt="Image">
                            </div>

                            <div class="tm-slideshow-description tm-slideshow-description-left tm-bg-highlight">
                                <h2 class="">{{$data->title}}</h2>
                                <p>{{$data->describe}}</p>
                                <a href="#" class="text-uppercase tm-btn tm-btn-white tm-btn-white-highlight">{{date('Y年m月s日',$data->timestamp)}}</a>
                            </div>

                        </section>
                @endif
        @endforeach
        </div>

        <footer class="tm-container-outer">
            <p class="mb-0">Copyright &copy; 2018.Company name All rights reserved.</p>
        </footer>
    </div>
</div> <!-- .main-content -->
</body>
</html>