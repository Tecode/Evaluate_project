<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>个人资料修改</title>
		<style>
			body{
				margin: 0;
				padding: 0;
			}
		header{
			border:0;
			height: 100px;
			width: 100%;
			
			padding-right: 0px;
		}
			header>.pic{
				width: auto;
			}
			.pic img{ 
				display:block;
		        margin: 0px auto;
			}
			section{
				border: 100px;
				height: 560px;
				width: 100%;
				
				margin: 0 auto;
				
			}
			section>div{
				float:left;
				margin-left: 300px;
				margin: auto;
				padding: 20px;
			}
			.input{
				padding: 20px;
			}
			.title{
				width:150px;
				height:100px;
				text-align:center;
				float:left;
				
				margin-left: 200px;
				line-height: 100px;
				
				box-shadow: 0 0 0 4px rgba(255,255,255,1);
			  transition: background 1s, color 0.5s;   
			}
			.title a {
				margin-top: 25px;
				font-size: 24px;
				font-weight:bold;
			
				color:white;
				text-decoration: none;
			}
			.title a span{
				
				font-size: 40px;
				font-weight:bold;
				color:white;
				margin-bottom:20px ;
				text-decoration: none;
			}
			.title:hover{
				background:salmon;
			    color: #64bb5d;
			}
			.title:hover{
				animation: spinAround 4s linear infinite;
			}
			@keyframes spinAround {
			    padding:20px;
			}
             .add{
            /*padding-top:25px;*/
            margin-top:0px;
            margin-bottom: 50px;
            font-size:x-large;
            color:ghostwhite;
            font:bolder 10px;
            padding-bottom:15px;
            }

		</style>
	</head>
	<body background="{{asset('imgs/load.jpg')}}">
		<header>
			<div class="title">
				<a href="index.html">
				<span class="add" style="width:100%;text-align:center;display:block;">改资料</span>
				</a>
			</div>
			<div class="pic">
			<img src=""/>
			</div>
			
		</header>
		<section>
			<center>
			<div class="input">
				<label id="yonghu" style="font-size: 18px;font-family:楷体;padding-left: 16px;color:	#8B795E;">用户名：</label>
				<input type="text" id="usename" placeholder="XXX" style="width: 300px;height: 20px;border:hidden; background-color:#EEE5DE ;" />
			</div>
			</center>
			<center>
			<div class="input">
				<label id="xingbie" style="font-size: 18px;font-family:楷体;padding-left: 32px;color:	#8B795E;">性别：</label>
				<input type="text" id="xingbie" placeholder="男" style="width: 300px;height: 20px;border:hidden; background-color:#EEE5DE ;" />
			</div>
			</center>
			<center>
			<div class="input">
				<label id="nianling" style="font-size: 18px;font-family:楷体;padding-left: 16px;color:	#8B795E;">年龄：</label>
				<input type="text" id="nianling" placeholder="18" style="width: 300px;height: 20px;border:hidden; background-color:#EEE5DE ;" />
			</div>
			</center>
			<center>
			<div  class="input">
				<label id="nicheng" style="font-size: 18px;font-family:楷体;padding-left: 32px;color:#8B795E;">昵称：</label>
				<input type="text" id="nicheng" placeholder="小小良" style="width: 300px;height: 20px;border:hidden; background-color:#EEE5DE" />
			</div>
			</center>
			<center>
			<div class="input">
				<label id="gexing"style="font-size: 18px;font-family:楷体;color:	#8B795E;">个性签名：</label>
				<input type="text" id="gexing" placeholder="我就是爱音乐，别叫我停下来!" style="width: 300px;height: 20px;border:hidden; background-color:#EEE5DE" />
			</div>
			</center>
			<center>
			<div class="input">
				<label id="mima"style="font-size: 18px;font-family:楷体;padding-left: 32px;color:	#8B795E;">密码：</label>
				<input type="text" id="mima" placeholder="*********" style="width: 300px;height: 20px;border:hidden; background-color:#EEE5DE" />
			</div>
			</center>
			<center>
			<div class="input">
				<label id="shouji"style="font-size: 18px;font-family:楷体;color:	#8B795E;">手机号码：</label>
				<input type="text" id="shouji" placeholder="189XXXX7786" style="width: 300px;height: 20px;border:hidden; background-color:#EEE5DE" />
			</div>
			</center>
		</section>
	</body>
</html>
