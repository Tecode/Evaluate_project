<?php

// 前台路由
Route::group(['namespace' => 'Front', 'prefix' => ''], function () {
    // 注册成功
    Route::get('/', 'HomeController@index');
    Route::get('/comment', 'HomeController@comment');
    Route::get('/write', 'HomeController@write');
    Route::get('/info', 'HomeController@info');
});
    //登录才可以访问的路由
Route::group(['middleware' => ['web', 'login'], 'namespace' => 'Front', 'prefix' => ''], function () {
    Route::post('/comment', 'HomeController@commentInput');
});
// 后台路由
Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::get('/home', 'HomeController@index');
    Route::post('/home/{id}', 'HomeController@deletCommon');
});

Auth::routes();