<?php

namespace App\Http\Controllers\Front;

use App\Http\Model\Images;
use App\Http\Model\Scenic;
use App\Http\Model\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // 中间件（如果没有登录会跳转页面到登录页）
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        function createData($data) {
            foreach ($data as $item) {
                $item['imgData'] = Images::where('scenic_id', $item['id'])->first();
            }
            return $data;
        }
        return view('front.index', ['indexData'=> createData(Scenic::where('other', '1')->get())]);
    }
    public function comment()
    {
        return view('front.comment', ['data' => Comment::where('show', 1)->get()]);
    }
    public function commentInput(Request $request)
    {
        $db = Comment::insert(
            [
                'name' => Auth::user()->name,
                'content' =>  $request['content'],
                'time' => time()
            ]);
        return view('front.comment', ['data' => Comment::where('show', 1)->get()]);
    }
    public function write(Request $request)
    {
        return view('front.write');
    }
    public function info(Request $request)
    {
        return view('front.info');
    }
}
