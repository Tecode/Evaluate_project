<?php

namespace App\Http\Controllers\Admin;

use App\Http\Model\Comment;
use App\Http\Model\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return view('admin.home', ['commentData' => Comment::where('show', 1)->get()]);
    }
    public function deletCommon($id) {
        $db = Comment::where('id', $id);
        $ok = $db->update(['show' => '0']);
        dd($ok);
        if($ok) {
            return ok;
        }
    }
}
